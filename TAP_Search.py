import pyvo as vo
import astropy.coordinates as coord
from astropy.coordinates import SkyCoord
from astropy import units as u
import jdutil
import math

heasarc = ''

def find_tap_services(search_string):
    tap_services = vo.regsearch(servicetype='tap', keywords=[search_string])
    return tap_services


def find_table_columns(relevant_tables):
    global heasarc
    heasarc = find_tap_services("heasarc")[0]

    tables = heasarc.service.tables

    #for tableName in tables.keys():
        #if tableName in relevant_tables:
            #print(tableName)
            #tables[tableName].describe()
            #print("Columns={}".format(sorted([k.name for k in tables[tableName].columns])))
            #print("----")


def get_swift_coords(year, month, day):
    global heasarc
    relevant_tables = ["swiftbalog", "swiftgrb", "swiftmastr", "swifttdrss", "swiftuvlog", "swiftxrlog"]
    find_table_columns(relevant_tables)
    image_services = vo.regsearch(servicetype='image', keywords=["Swift"])
    query = "SELECT archive_date, start_time, stop_time, name, lii, bii FROM swiftmastr where FLOOR(start_time) = " + \
            str(jdutil.jd_to_mjd(jdutil.date_to_jd(year, month, day))) + " order by archive_date desc"
    results = heasarc.service.run_async(query)
    results.to_table()
    final_images = []
    final_results = {}
    final_results["results"] = []
    for value in results:
        coords_gal = SkyCoord(frame="galactic", l=value["lii"] * u.degree, b=value["bii"] * u.degree)
        coords = coords_gal.transform_to(SkyCoord(ra= 1*u.degree, dec=1*u.degree))
        i = 0
        for service in image_services:
            if i == 5:
                resultset = service.search(pos=coords, size=.5)
                for result in resultset:
                    if math.floor(result["start_time"]) == math.floor(value["start_time"]):
                        final_images.append(result)
            i += 1
    for image in final_images:
        final_year, final_month, final_day = jdutil.jd_to_date(jdutil.mjd_to_jd(image["start_time"]))
        f, i = math.modf(final_day)
        final_hour, final_minutes, final_seconds, final_microseconds = jdutil.days_to_hmsm(f)
        image_json = {}
        image_json["image_url"] = image["SIA_reference"].decode('UTF-8')
        image_json["image_format"] = image["SIA_format"].decode('UTF-8')
        image_json["target_name"] = image["target_name"].decode('UTF-8')
        image_json["year"] = final_year
        image_json["month"] = final_month
        image_json["day"] = i
        image_json["hour"] = final_hour
        image_json["minutes"] = final_minutes
        image_json["seconds"] = final_seconds
        image_json["microseconds"] = final_microseconds
        final_results["results"].append(image_json)
    return final_results

def get_fermi_coords(date):
    relevant_tables = ["fermi2favs", "fermi3fgl", "fermi3fhl", "fermifhl", "fermigbrst", "fermigdays", "fermigsol",
                       "fermigtrig", "fermil2psr", "fermilac", "fermilasp", "fermilatra", "fermilbsl", "fermilgrb",
                       "fermilhesc", "fermille", "fermilpsc", "fermilweek"]
    find_table_columns(relevant_tables)


def get_integral_coords(date):
    relevant_tables = ["integralao"]
    find_table_columns(relevant_tables)
