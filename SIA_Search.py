import pyvo as vo
import astropy.coordinates as coord

coords = coord.SkyCoord.from_name("m51")
size = .02
#image_services = vo.regsearch(servicetype='image', keywords=["skyview"])
#print(image_services)

#for service in image_services:
#    print(service)
#    resultset = service.search(pos=coords,size=0.7)
#    print(resultset)

image_services = vo.regsearch(servicetype='image', keywords=["XMM-Newton"])
print(image_services)
i = 0
for service in image_services:
    print("Service " + str(i) + ": ")
    print(service)
    if i != 4:
        resultset = service.search(pos=coords,size=size)
        print(resultset)
        for result in resultset:
            print(result)
    i += 1

image_services = vo.regsearch(servicetype='image', keywords=["Swift"])
print(image_services)

i = 0
for service in image_services:
    print("Service " + str(i) + ": ")
    print(service)
    if i != 4:
        resultset = service.search(pos=coords,size=size)
        print(resultset)
        for result in resultset:
            print(result)
    i += 1

image_services = vo.regsearch(servicetype='image', keywords=["NuSTAR"])
print(image_services)

i = 0
for service in image_services:
    print("Service " + str(i) + ": ")
    print(service)
    resultset = service.search(pos=coords,size=size)
    print(resultset)
    for result in resultset:
        print(result)
    i += 1

image_services = vo.regsearch(servicetype='image', keywords=["INTEGRAL"])
print(image_services)

i = 0
for service in image_services:
    print("Service " + str(i) + ": ")
    print(service)
    resultset = service.search(pos=coords,size=size)
    print(resultset)
    for result in resultset:
        print(result)
    i += 1

image_services = vo.regsearch(servicetype='image', keywords=["Fermi"])
print(image_services)

i = 0
for service in image_services:
    print("Service " + str(i) + ": ")
    print(service)
    resultset = service.search(pos=coords,size=size)
    print(resultset)
    for result in resultset:
        print(result)
    i += 1

image_services = vo.regsearch(servicetype='image', keywords=["Chandra"])
print(image_services)

i = 0
for service in image_services:
    print("Service " + str(i) + ": ")
    print(service)
    if i != 10 and i != 13:
        resultset = service.search(pos=coords,size=size)
        print(resultset)
        for result in resultset:
            print(result)
    i += 1

