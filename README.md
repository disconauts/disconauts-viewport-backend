In virtual environment,

sudo apt install python3-pip
pip install flask flask-restful gunicorn

Install nginx,

sudo apt install nginx

Create a service file to host the api locally using gunicorn,

backend.service

Copy the service file to /etc/systemd/system/backend.service,

cp backend.service /etc/systemd/system/backend.service

Start the service,

sudo systemctl start backend.service

Now we have the API listening on localhost:5000.

Create an NGINX configuration file, and start NGINX to proxy
heasarc.dudelab.net/api to localhost:5000

Copy to sites-available, link to sites-enabled, and reload nginx.

