from flask import request, Flask, send_from_directory
from flask_restful import Resource, Api
from marshmallow import Schema, fields
import TAP_Search
import requests
import os
from astropy.io import fits
from flask_cors import CORS

app = Flask(__name__)
api = Api(app)
CORS(app, origins=[], automatic_options=True)

class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

api.add_resource(HelloWorld, '/')


class SwiftQuery(Schema):
    year = fields.Int(required=True)
    month = fields.Int(required=True)
    day = fields.Int(required=True)


class FITSUtilQuery(Schema):
    url = fields.URL(required=True)

schema = SwiftQuery()
fits_schema = FITSUtilQuery()


class Swift(Resource):
    def get(self):
        errors = schema.validate(request.args)
        if errors:
            abort(400, str(errors))
        results = TAP_Search.get_swift_coords(int(request.args["year"]), int(request.args["month"]), int(request.args["day"]))
        return results


class FITSUtil(Resource):
    def get(self):
        errors = fits_schema.validate(request.args)
        if errors:
            abort(400, str(errors))
        os.system("curl "+request.args["url"]+" -o output.gz")
        os.system("gzip -d output.gz")
        return send_from_directory(os.getcwd(), "output", as_attachment=True)

api.add_resource(Swift, '/Swift')
api.add_resource(FITSUtil, '/FITSUtil')
if __name__ == '__main__':
    app.run()

